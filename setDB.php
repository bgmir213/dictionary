<?php
require_once 'config.php';

// Полная очистка имеющейся БД
$tables = array();
$res = $mysqli->query('show tables');
while($row = $res->fetch_assoc()){
    $tables[] = $row['Tables_in_'.$dbName];
}
print_r($tables);

$res = $mysqli->query("SET FOREIGN_KEY_CHECKS=0");
foreach($tables as $tableName){
    $res = $mysqli->query("DROP TABLE `$tableName`");
}

$res = $mysqli->query("SET FOREIGN_KEY_CHECKS=1");

// Создание структуры
$res = $mysqli->query("CREATE TABLE `words` (
    `id` int(11) NOT NULL,
    `ru` varchar(255) NOT NULL,
    `en` varchar(255) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$res = $mysqli->query("ALTER TABLE `words` ADD PRIMARY KEY (`id`);");

$res = $mysqli->query("ALTER TABLE `words` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;");


// Добавление данных
$res = $mysqli->query("
INSERT INTO `words` (`id`, `ru`, `en`) VALUES
(1, 'предупреждение', 'alert'),
(2, 'прекратить', 'abort'),
(3, 'массив', 'array'),
(4, 'стрелка', 'arrow'),
(5, 'цифра', 'digit'),
(6, 'диск', 'disc');
");
